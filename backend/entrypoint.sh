#!/bin/sh

echo "Awaiting database connection "

while ! mysqladmin ping -h "database" --silent; do
    echo ". "
    sleep 1
done

echo "Database is ready and reachable at: "
echo "mysql://${SYMFONY_USER}:${SYMFONY_PASSWORD}@database:3306/${SYMFONY_DATABASE}"

echo "Executing composer install"
composer install

echo "Executing migrations"
php bin/console doctrine:migrations:migrate --no-interaction

if [ "$APP_ENV" = "dev" ]; then
    echo "Executing commands"
    php bin/console app:RSS
    php bin/console app:NEWSAPI
    php bin/console app:SPACENEWS
fi

exec php-fpm