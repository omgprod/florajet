<?php

namespace App\Controller;

use App\Entity\NewsData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Attributes as OA;

#[Route('/api/news', name: 'news_api_')]
class NewsApiController extends AbstractController
{
    #[OA\Tag(name: 'Api-News')]
    #[Route('', name: 'list', methods: ['GET'])]
    public function index(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer): JsonResponse
    {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);
        $sortBy = $request->query->get('sortBy', 'id');
        $order = $request->query->get('order', 'asc');
        $orderBy = ["sort" => $sortBy, "order" => $order];
        $criteria = $request->get("criteria", []);

        $result = $entityManager->getRepository(NewsData::class)->getList($criteria, $orderBy, $page, $limit);

        $json = $serializer->serialize($result, 'json', ['groups' => 'get']);

        return new JsonResponse(data: json_decode($json));
    }

    #[OA\Tag(name: 'Api-News')]
    #[Route('/{id}', name: 'getOne', requirements: ['id' => '^[1-9]\d*$'], methods: ['GET'])]
    public function getOne(NewsData|null $news)
    {
        if($news === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        return $this->json($news, Response::HTTP_OK);
    }

    #[OA\Tag(name: 'Api-News')]
    #[Route('/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(NewsData|null $data, EntityManagerInterface $entityManager): Response
    {

        if($data === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        $entityManager->remove($data);
        $entityManager->flush();

        return new Response('Nouvelle supprimée');
    }

    #[OA\Tag(name: 'Api-News')]
    #[Route('', name: 'create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $data = $request->getContent();
        $news = $serializer->deserialize($data, NewsData::class, 'json');
        
        $errors = $validator->validate($news);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }
        
        $entityManager->persist($news);
        $entityManager->flush();
        
        return new JsonResponse(['message' => 'Nouvelle créée'], Response::HTTP_CREATED);
    }

    #[OA\Tag(name: 'Api-News')]
    #[Route('/{id}', name: 'update', methods: ['PUT'])]
    public function update(NewsData|null $news, Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {

        if($news === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        $data = $request->getContent();
        $new = $serializer->deserialize($data, NewsData::class, 'json');
        
        $news->setTitle($new->getTitle() ?? $news->getTitle());
        $news->setUrl($new->getUrl() ?? $news->getUrl());
        $news->setUrlImage($new->getUrlImage() ?? $news->getUrlImage());
        $news->setPublishedAt($new->getPublishedAt() ?? $news->getPublishedAt());

        $errors = $validator->validate($news);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_BAD_REQUEST);
        }
        
        $entityManager->persist($news);
        $entityManager->flush();
        
        return new JsonResponse(['message' => 'Nouvelle modifiée'], Response::HTTP_OK);
    }

}
