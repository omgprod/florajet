<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class LoginController extends AbstractController
{
     #[Route('/api/register', name: 'app_registration', methods: ['POST'])]
     #[OA\Response(
        response: 200,
        description: 'Return string',
        content: new OA\MediaType(
            mediaType: 'application/json',
            schema: new OA\Schema(type: 'string')
        )
    )]
    #[OA\Parameter(
        name: 'email',
        in: 'query',
        description: 'The field used to set the user email',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Parameter(
        name: 'password',
        in: 'query',
        description: 'The field used to set the user password',
        schema: new OA\Schema(type: 'string')
    )]
    #[OA\Tag(name: 'Authentication')]
    #[Security(name: 'Bearer')]
     public function register(Request $request, EntityManagerInterface $entityManager)
     {
        $data = $request->request->all();
        dump($data);
        if(!isset($data['email']) || !isset($data['password'])){
            return $this->json(['message' => 'Email and password are required!']);
        }

        $email = $data['email'];
        $password = $data['password'];

        $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
        if($user){
            return $this->json(['message' => 'Email already exists!']);
        }

        $password = password_hash($password, PASSWORD_DEFAULT);
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->json(['message' => 'User registered successfully!']);
    }
    
    #[Route('/api/renew-token', name: 'app_renew_token', methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: 'Return string',
        content: new OA\MediaType(
            mediaType: 'application/json',
            schema: new OA\Schema(type: 'string')
        )
    )]
    #[OA\Parameter(
        name: 'email',
        in: 'query',
        description: 'The field used to set the user email',
        schema: new OA\Schema(type:'string')
    )]
    #[OA\Parameter(
        name: 'password',
        in: 'query',
        description: 'The field used to set the user password',
        schema: new OA\Schema(type:'string')
    )]
    #[OA\Tag(name: 'Authentication')]
    #[Security(name: 'Bearer')]
     public function renewToken(Request $request, EntityManagerInterface $entityManager)
        {
            $data = $request->request->all();
            if(!isset($data['email']) || !isset($data['password'])){
                return $this->json(['message' => 'Email and password are required!']);
            }
    
            $email = $data['email'];
            $password = $data['password'];
    
            $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
            if(!$user){
                return $this->json(['message' => 'User not found!']);
            }
    
            if(!password_verify($password, $user->getPassword())){
                return $this->json(['message' => 'Invalid password!']);
            }
    
            return $this->json(['message' => 'Token renewed successfully!']);
        }

        #[Route('/api/login-keycloak', name: 'app_login', methods: ['POST'])]
        #[OA\Response(
            response: 200,
            description: 'Return string',
            content: new OA\MediaType(
                mediaType: 'application/json',
                schema: new OA\Schema(type: 'string')
            )
        )]
        #[OA\Parameter(
            name: 'email',
            in: 'query',
            description: 'The field
            used to set the user email',
            schema: new OA\Schema(type:'string')
        )]
        #[OA\Parameter(
            name: 'password',
            in: 'query',
            description: 'The field
            used to set the user password',
            schema: new OA\Schema(type:'string')
        )]
        #[OA\Tag(name: 'Authentication')]
        #[Security(name: 'Bearer')]
         public function login(Request $request, EntityManagerInterface $entityManager)
         {
            $data = $request->request->all();
            if(!isset($data['email']) ||!isset($data['password'])){
                return $this->json(['message' => 'Email and password are required!']);
            }

            $email = $data['email'];
            $password = $data['password'];

            $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
            if(!$user){
                return $this->json(['message' => 'User not found!']);
            }
            if(!password_verify($password, $user->getPassword())){
                return $this->json(['message' => 'Invalid password!']);
            }
            return $this->json(['token' => $user->getToken()]);
        }

}