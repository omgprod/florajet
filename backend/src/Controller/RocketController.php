<?php

namespace App\Controller;

use App\Entity\SpaceData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Attributes as OA;

#[Route('/api/rocket-news', name: 'news_rocket_')]
class RocketController extends AbstractController
{
    #[OA\Tag(name: 'Rocket-News')]
    #[Route('', name: 'list', methods: ['GET'])]
    public function index(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer): JsonResponse
    {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);
        $sortBy = $request->query->get('sortBy', 'id');
        $order = $request->query->get('order', 'asc');
        $orderBy = ["sort" => $sortBy, "order" => $order];
        $criteria = $request->get("criteria", []);

        $result = $entityManager->getRepository(SpaceData::class)->getList($criteria, $orderBy, $page, $limit);

        $json = $serializer->serialize($result, 'json', ['groups' => 'get']);

        return new JsonResponse(data: json_decode($json));
    }

    #[OA\Tag(name: 'Rocket-News')]
    #[Route('/{id}', name: 'getOne', requirements: ['id' => '^[1-9]\d*$'], methods: ['GET'])]
    public function getOne(SpaceData|null $news)
    {

        if($news === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        return $this->json($news, Response::HTTP_OK);
    }

    #[OA\Tag(name: 'Rocket-News')]
    #[Route('/{id}', name: 'delete', methods: ['DELETE'])]
    public function delete(SpaceData|null $data, EntityManagerInterface $entityManager): JsonResponse
    {

        if($data === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        $entityManager->remove($data);
        $entityManager->flush();

        return $this->json('Nouvelle supprimée', Response::HTTP_OK);
    }

    #[OA\Tag(name: 'Rocket-News')]
    #[Route('', name: 'create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $data = $request->getContent();
        $rocketData = $serializer->deserialize($data, SpaceData::class, 'json');
        
        $errors = $validator->validate($rocketData);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse($errorMessages, Response::HTTP_BAD_REQUEST);
        }
        
        dd($rocketData);

    
        $entityManager->persist($rocketData);
        $entityManager->flush();
        
        return new JsonResponse('Nouvelle créée', Response::HTTP_CREATED);
    }

    #[OA\Tag(name: 'Rocket-News')]
    #[Route('/{id}', name: 'update', methods: ['PUT'])]
    public function update(SpaceData|null $new, Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        $data = $request->getContent();
        $rocketData = $serializer->deserialize($data, SpaceData::class, 'json');

        if($new === null) {
            return $this->json("Aucun résultat", Response::HTTP_NOT_FOUND);
        }

        $new->setTitle($rocketData->getTitle() ?? $new->getTitle());
        $new->setUrl($rocketData->getUrl() ?? $new->getUrl());
        $new->setUrl($rocketData->getUrlImage() ?? $new->getUrlImage());
        $new->setNewsSite($rocketData->getNewsSite() ?? $new->getNewsSite());
        $new->setPublishedAt($rocketData->getPublishedAt() ?? $new->getPublishedAt());
        $new->setUpdatedAt($rocketData->getUpdatedAt() ?? $new->getUpdatedAt());
        $new->setFeatured($rocketData->isFeatured() ?? $new->isFeatured());
        
        $errors = $validator->validate($new);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return new JsonResponse($errorMessages, Response::HTTP_BAD_REQUEST);
        }
        
        $entityManager->persist($new);
        $entityManager->flush();
        
        return new JsonResponse('Nouvelle modifée', Response::HTTP_OK);
    }

}