<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
class UserController extends AbstractController
{
    #[Route('/api/users', name: 'user', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Returns the list of users',
        //content: new Model(type: User::class)
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class))
        )
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function index(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    { 
        $users = $entityManager->getRepository(User::class)->findAll();
        $json = $serializer->serialize($users, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/users', name: 'create-user', methods: ['POST'])]
    #[OA\Response(
        response: 200,
        description: 'Create new user',
        content: new Model(type: User::class),
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function create(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    { 
        $user = new User();
        $user->setEmail($request->request->get('email'));
        $user->setPassword($request->request->get('password'));
        $entityManager->persist($user);
        $entityManager->flush();
        $json = $serializer->serialize($user, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/users/{id}', name: 'get-user', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get user',
        content: new Model(type: User::class),
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function findOne(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, $id)
    { 
        $user = $entityManager->getRepository(User::class)->find($id);
        $json = $serializer->serialize($user, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/users/{id}', name: 'update-user', methods: ['PUT'])]
    #[OA\Response(
        response: 200,
        description: 'Update user',
        content: new Model(type: User::class),
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function update(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, $id)
    { 
        $user = $entityManager->getRepository(User::class)->find($id);
        $user->setEmail($request->request->get('email'));
        $user->setPassword($request->request->get('password'));
        $entityManager->persist($user);
        $entityManager->flush();
        $json = $serializer->serialize($user, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/users/{id}', name: 'delete-user', methods: ['DELETE'])]
    #[OA\Response(
        response: 200,
        description: 'Delete user',
        content: new Model(type: User::class),
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function delete(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, $id)
    { 
        $user = $entityManager->getRepository(User::class)->find($id);
        $entityManager->remove($user);
        $entityManager->flush();
        $json = $serializer->serialize($user, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    #[Route('/api/users/search', name: 'user-search', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Listing user with pagination',
        content: new OA\JsonContent(type: 'array',
            items: new OA\Items(ref: new Model(type: User::class))
        )
    )]
    #[OA\Tag(name: 'Users')]
    #[Security(name: 'Bearer')]
    public function search(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer)
    { 
        $rowPerPage = $request->query->getInt('rowPerPage', 10);
        $page = $request->query->getInt('page', 1);
        $users = $entityManager->getRepository(User::class)->findBy([], null, $rowPerPage, ($page - 1) * $rowPerPage);
        $json = $serializer->serialize($users, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }
}
