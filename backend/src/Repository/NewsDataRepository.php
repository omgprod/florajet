<?php

namespace App\Repository;

use App\Entity\NewsData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<NewsData>
 *
 * @method NewsData|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsData|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsData[]    findAll()
 * @method NewsData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsData::class);
    }

    public function getList(array $criteria, array $orderBy, int $page, int $limit): array
    {
        $qb = $this->createQueryBuilder('news_data');

        $countQb = clone $qb;
        $countQb->select('COUNT(news_data.id)');
        $total = $countQb->getQuery()->getSingleScalarResult();

        $qb->orderBy('news_data.' . $orderBy['sort'], $orderBy['order'])
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return [
            'list' => $qb->getQuery()->getResult(),
            'total' => $total,
        ];
    }
}
