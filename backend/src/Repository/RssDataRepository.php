<?php

namespace App\Repository;

use App\Entity\RssData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RssData>
 *
 * @method RssData|null find($id, $lockMode = null, $lockVersion = null)
 * @method RssData|null findOneBy(array $criteria, array $orderBy = null)
 * @method RssData[]    findAll()
 * @method RssData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RssDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RssData::class);
    }
    public function getList(array $criteria, array $orderBy, int $page, int $limit): array
    {
        $qb = $this->createQueryBuilder('rss_data');

        $countQb = clone $qb;
        $countQb->select('COUNT(rss_data.id)');
        $total = $countQb->getQuery()->getSingleScalarResult();

        $qb->orderBy('rss_data.' . $orderBy['sort'], $orderBy['order'])
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return [
            'list' => $qb->getQuery()->getResult(),
            'total' => $total,
        ];
    }
}
