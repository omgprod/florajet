<?php

namespace App\Repository;

use App\Entity\SpaceData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SpaceData>
 *
 * @method SpaceData|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpaceData|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpaceData[]    findAll()
 * @method SpaceData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpaceDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpaceData::class);
    }
    public function getList(array $criteria, array $orderBy, int $page, int $limit): array
    {
        $qb = $this->createQueryBuilder('rocket_data');

        $countQb = clone $qb;
        $countQb->select('COUNT(rocket_data.id)');
        $total = $countQb->getQuery()->getSingleScalarResult();

        $qb->orderBy('rocket_data.' . $orderBy['sort'], $orderBy['order'] === 'asc' ? 'ASC' : 'DESC')
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return [
            'list' => $qb->getQuery()->getResult(),
            'total' => $total,
        ];
    }
}
