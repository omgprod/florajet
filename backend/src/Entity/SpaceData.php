<?php

namespace App\Entity;

use App\Repository\SpaceDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SpaceDataRepository::class)]
class SpaceData
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['get'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['get'])]
    private ?int $originalId = null;

    #[ORM\Column(length: 255)]
    #[Groups(['get'])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['get'])]
    private ?string $url = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['get'])]
    private ?string $urlImage = null;

    #[ORM\Column(length: 55)]
    #[Groups(['get'])]
    private ?string $newsSite = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['get'])]
    private ?string $summary = null;

    #[ORM\Column]
    #[Groups(['get'])]
    private ?\DateTimeImmutable $publishedAt = null;

    #[ORM\Column]
    #[Groups(['get'])]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\Column]
    #[Groups(['get'])]
    private ?bool $featured = null;

    #[ORM\Column]
    #[Groups(['get'])]
    private array $launches = [];

    #[ORM\Column]
    #[Groups(['get'])]
    private array $events = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalId(): ?int
    {
        return $this->originalId;
    }

    public function setOriginalId(int $originalId): static
    {
        $this->originalId = $originalId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getUrlImage(): ?string
    {
        return $this->urlImage;
    }

    public function setUrlImage(string $urlImage): static
    {
        $this->urlImage = $urlImage;

        return $this;
    }

    public function getNewsSite(): ?string
    {
        return $this->newsSite;
    }

    public function setNewsSite(string $newsSite): static
    {
        $this->newsSite = $newsSite;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): static
    {
        $this->summary = $summary;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeImmutable $publishedAt): static
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(bool $featured): static
    {
        $this->featured = $featured;

        return $this;
    }

    public function getLaunches(): array
    {
        return $this->launches;
    }

    public function setLaunches(array $launches): static
    {
        $this->launches = $launches;

        return $this;
    }

    public function getEvents(): array
    {
        return $this->events;
    }

    public function setEvents(array $events): static
    {
        $this->events = $events;

        return $this;
    }
}
