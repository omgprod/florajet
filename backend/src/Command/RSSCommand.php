<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\RSSAggregatorService;
use Doctrine\DBAL\DriverManager;

#[AsCommand(
    name: 'app:RSS',
    description: 'Add a short description for your command',
)]
class RSSCommand extends Command
{

    private $rssService;

    public function __construct(RSSAggregatorService $rssService)
    {
        $this->rssService = $rssService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->note("Begin fetching datas from RSS feeds");


        $rssFeeds = [
            'http://www.lemonde.fr/rss/une.xml',
        ];

        $counter = $this->rssService->aggregateRSSFeeds($rssFeeds);

        $io->success($counter . ' Items added from RSS feeds');

        return Command::SUCCESS;
    }
}
