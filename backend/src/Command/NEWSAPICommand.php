<?php

namespace App\Command;

use App\Entity\NewsData;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\APIAggregatorService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;


#[AsCommand(
    name: 'app:NEWSAPI',
    description: 'Add a short description for your command',
)]
class NEWSAPICommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Connection             $connection,
    )
    {
        parent::__construct();

    }

    protected function configure(): void
    {
        $this
            ->addArgument('token', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->note("Begin fetching datas from API");

        $count = 0;

        $apiEndpoints = [
            "https://newsapi.org/v2/top-headlines?country=fr&apiKey=2315be7da4054189b319d6879669379c",
            "https://saurav.tech/NewsAPI/top-headlines/category/health/fr.json"
        ];
        $io->note("Fetching datas from " . count($apiEndpoints) . " API");


        $aggregator = new APIAggregatorService($apiEndpoints);
        
        $results = $aggregator->aggregateData();


        foreach ($results as $result) {
            $res = json_decode($result, true);
            foreach ($res["articles"] as $article) {
                $item = new NewsData();
                $item->setTitle($article['title']);
                $item->setUrl($article['url']);
                $item->setUrlImage($article['urlImage'] ?? "");
                $item->setSource($article['source'] ?? []);
                $item->setDescription($article['description'] ?? "");
                $item->setAuthor($article['author']);
                $item->setPublishedAt($article['publishedAt']);
                $item->setContent($article['content'] ?? "");
                $this->entityManager->persist($item);
                $count++;
            }
            $this->entityManager->flush();
            $io->success($count . " Items saved to database");

        }
        $io->success('API Command executed successfully!');
        return Command::SUCCESS;
    }
}
