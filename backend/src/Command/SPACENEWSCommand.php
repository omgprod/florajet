<?php

namespace App\Command;

use App\Entity\NewsData;
use App\Entity\SpaceData;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\APIAggregatorService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

#[AsCommand(
    name: 'app:SPACENEWS',
    description: 'Add a short description for your command',
)]
class SPACENEWSCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Connection             $connection,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->note("Begin fetching datas from API");

        $count = 0;

        $apiEndpoints = [
            "https://api.spaceflightnewsapi.net/v3/articles",
        ];
        $io->note("Fetching datas from " . count($apiEndpoints) . " API");


        $aggregator = new APIAggregatorService($apiEndpoints);
        
        $results = $aggregator->aggregateData();

        foreach ($results as $result) {
            $res = json_decode($result, true);
            foreach ($res as $article) {
                $item = new SpaceData();
                $item->setOriginalId($article['id']);
                $item->setTitle($article['title']);
                $item->setUrl($article['url']);
                $item->setNewsSite($article['newsSite']);
                $item->setUrlImage($article['urlImage'] ?? "");
                $item->setSummary($article['summary'] ?? []);
                $item->setFeatured($article['featured'] ?? false);
                $item->setLaunches($article['launches']);
                $item->setEvents($article['events'] ?? []);
                $item->setPublishedAt(new \DateTimeImmutable($article['publishedAt']) ?? new \DateTimeImmutable());
                $item->setUpdatedAt(new \DateTimeImmutable($article['updatedAt']) ?? new \DateTimeImmutable());
                $this->entityManager->persist($item);
                $count++;
            }
            $this->entityManager->flush();
            $io->success($count . " Items saved to database");

        }
        $io->success('API Command executed successfully!');
        return Command::SUCCESS;
    }
}
