<?php

namespace App\DataFixtures;

use App\Entity\Flux;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $user = new User();
        $user->setEmail('master@world.com')
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->userPasswordHasher->hashPassword($user, "Ledododududodudindon"));
        $manager->persist($user);
        
        $user = new User();
        $user->setEmail('JR')
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->userPasswordHasher->hashPassword($user, "JR"));
        $manager->persist($user);



        for ($i = 0; $i < 100; $i++) {
            $user = new User();
            $user->setEmail($faker->email())
                ->setRoles(["ROLE_USER"])
                ->setPassword($this->userPasswordHasher->hashPassword($user, "Ledododududodudindon"));
            $manager->persist($user);
        }

        for ($i = 0; $i < 100; $i++) {
            $flux = new Flux();
            $flux->setName($faker->name())
                ->setUrl($faker->url())
                ->setPass($faker->password())
                ->setDir("./public/uploads/". $faker->name(). "/");
            $manager->persist($flux);
        }

        $manager->flush();
    }
}