<?php

namespace App\Service;

use Doctrine\DBAL\Connection;

class RSSAggregatorService
{
    private $dbConnection;

    public function __construct(Connection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function aggregateRSSFeeds($rssFeeds)
    {
        $counter = 0;
        foreach ($rssFeeds as $rssFeed) {
            $rssData = $this->fetchRSSData($rssFeed);
            $counter = $this->saveRSSDataToDatabase($rssData);
        }
        return $counter;
    }

    private function fetchRSSData($rssFeed)
    {
        $rssData = [];

        $xml = simplexml_load_file($rssFeed);
        if ($xml) {
            foreach ($xml->channel->item as $item) {
                $rssData[] = [
                    'title' => htmlspecialchars((string) $item->title ?? ""),
                    'link' => htmlspecialchars((string) $item->link ?? ""),
                    'description' => htmlspecialchars((string) $item->description ?? ""),
                ];
            }
        }

        return $rssData;
    }

    private function saveRSSDataToDatabase($rssData, $truncate = false)
    {
        if(!!$truncate) $this->dbConnection->executeQuery("TRUNCATE TABLE rss_data");
        
        $stmt = $this->dbConnection->prepare("INSERT INTO rss_data (title, link, description) VALUES (:title, :link, :description)");

        $count = 0;

        foreach ($rssData as $item) {
            $stmt->executeQuery([
                'title' => $item['title'],
                'link' => $item['link'],
                'description' => $item['description'],
            ]);
            $count++;
        }
        return $count;
    }
}