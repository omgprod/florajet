<?php

namespace App\Service;

use GuzzleHttp\Client;

class APIAggregatorService
{
    private $apiEndpoints;

    public function __construct(array $apiEndpoints)
    {
        $this->apiEndpoints = $apiEndpoints;
    }

    public function aggregateData()
    {
        $results = [];

        foreach ($this->apiEndpoints as $endpoint) {
            $data = $this->fetchDataFromAPI($endpoint);
            $results[] = $data;
        }

        return $results;
    }

    private function fetchDataFromAPI($endpoint)
    {
        $client = new Client();
        $response = $client->get($endpoint);
        $data = $response->getBody()->getContents();

        return $data;
    }

    public function fetchAuthToken($endpoint, $username, $password, $key = 'token')
    {
        $client = new Client();
        $response = $client->post($endpoint, [
            'form_params' => [
                'username' => $username,
                'password' => $password,
            ],
        ]);
        $data = $response->getBody()->getContents();
        $token = json_decode($data, true)[$key];
        return $token;
    }

    private function fetchDataFromAPIWithBearer($endpoint, $authToken)
    {
        $client = new Client();
        $response = $client->get($endpoint, [
            'headers' => [
                'Authorization' => 'Bearer ' . $authToken,
            ],
        ]);
        $data = $response->getBody()->getContents();
        return $data;
    }
}