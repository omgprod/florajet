# FLORAJET DOCKERIZED 

- [![Docker](https://img.shields.io/badge/Docker-25.0.3-blue?logo=docker)](https://www.docker.com/)
- [![Docker Compose](https://img.shields.io/badge/Docker%20Compose-2.24.6-blue?logo=docker)](https://docs.docker.com/compose/)
- [![PHP](https://img.shields.io/badge/PHP-8.3-blue?logo=php)](https://www.php.net/)
- [![Nginx](https://img.shields.io/badge/Nginx-<VERSION>-blue?logo=nginx)](https://nginx.org/)
- [![MySQL](https://img.shields.io/badge/MySQL-8.3-blue?logo=mysql)](https://www.mysql.com/)
- [![Symfony](https://img.shields.io/badge/Symfony-7.0.5-blue?logo=symfony)](https://symfony.com/)

### Started 20/05/2024 - Last Update 22/05/2024

- [x] Aggregator API
- [x] Aggregator RSS
- [x] Aggregator API with Token
- [x] Controller CRUD
- [ ] Using Cache
- [ ] Using Queue
- [ ] Using Auth

## Requirements

- Docker
- Docker Compose

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository.
2. Navigate to the project directory.
3. Run `docker compose build && docker compose up -d && docker compose logs -f`
4. Or run `bin/project rebuild` to start the project.

### ROUTING

#### Backend: 
- http://localhost 
- http://localhost/api/doc

#### PHPMyAdmin: 
- http://localhost:8097 


### Bash commands: 
#### Go to project's root

- Run composer command in backend container :

```shell
bin/composer <composer params>
```

- Run symfony console command in backend container :

```shell
bin/console <console parameters>
```

- Run npm command in quasar container :

```shell
bin/npm <npm parameters>
```

- Run quick docker compose combos commmand :

```bash
bin/project -h
```
OR
```bash
bin/project <parameters>
```

### Run command into docker container
#### At any location

- List containers: 

```bash
docker ps
```

- Enter the container: 

```bash
docker exec -it <container_name> bash
```

- Run a command in the container: 

```bash
docker exec -it <container_name> <command>
```

## Credits

- HB - Haggerty Brian